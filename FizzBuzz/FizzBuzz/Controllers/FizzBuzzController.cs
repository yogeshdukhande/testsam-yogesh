﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzController.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using FizzBuzz.Common;
    using FizzBuzz.Models;
    using FizzBuzz.Repository;
    using FizzBuzzBusinessLayer;
    using StructureMap;

    /// <summary>
    /// Definition of FizzBuzzController class
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// Define businessLogic
        /// </summary>
        public readonly IRepository FizzBuzzStrategic;

        /// <summary>
        /// define FizzBuzzBusinessRule
        /// </summary>
        public readonly BaseFizzBuzz FizzBuzzBusinessRule;

        /// <summary>
        /// define FizzBuzzBusinessRule
        /// </summary>
        public readonly FizzBuzzBusinessLogic FizzBuzzBusiness;

        /// <summary>
        /// Define instances
        /// </summary>
        private IEnumerable<BaseFizzBuzz> instances;

        /// <summary>
        /// Initializes a new instance of the FizzBuzzController class.
        /// </summary>
        /// <param name="inputBusinessLogic">parameter inputBusinessLogic</param>
        /// <param name="fizzBuzzInstances">parameter fizzBuzzInstances</param>
        /// <param name="fizzBuzzBusinessLogic">parameter fizzBuzzBusinessLogic</param>
        public FizzBuzzController(IRepository inputBusinessLogic, IEnumerable<BaseFizzBuzz> fizzBuzzInstances, FizzBuzzBusinessLogic fizzBuzzBusinessLogic)
        {
            this.FizzBuzzStrategic = inputBusinessLogic;

            this.Instances = fizzBuzzInstances;

            this.FizzBuzzBusiness = fizzBuzzBusinessLogic;
        }

        /// <summary>
        /// Gets or sets Instances
        /// </summary>
        public IEnumerable<BaseFizzBuzz> Instances
        {
            get
            {
                return this.instances;
            }

            set
            {
                this.instances = value;     
            }
        }

        /// <summary>
        /// get IFizzBuzz Instance
        /// </summary>
        /// <returns>return IFizzBuzz</returns>
        public static IEnumerable<BaseFizzBuzz> GetInstance()
        {
            var container = new Container();

            var rules = container.GetAllInstances<BaseFizzBuzz>();

            return rules;
        }

        /// <summary>
        /// Action method to render the initial view.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        [HttpGet]
        public ActionResult FizzBuzzView()
        {
            return this.View("FizzBuzzView", new FizzBuzzModel());
        }

        /// <summary>
        /// Action method to render on Get Data button submit.
        /// </summary>
        /// <param name="inputpostModel">the input postModel</param>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "DisplayList")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzView(FizzBuzzModel inputpostModel)
        {
            FizzBuzzModel fizzBuzzModel = new FizzBuzzModel() { Number = inputpostModel.Number };
            fizzBuzzModel.Page.PageIndex = 1;

            if (ModelState.IsValid) 
            {
                this.FillNumberList(fizzBuzzModel); 
            }

            this.TempData["FizzBuzzPage"] = fizzBuzzModel.Page;
            return this.View("FizzBuzzView", fizzBuzzModel);
        }

        /// <summary>
        /// Action method to render on Previous button click.
        /// </summary>
        /// <param name="inputpostModel">the input postModel</param>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "Previous")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzViewPrevious(FizzBuzzModel inputpostModel)
        {
            FizzBuzzModel fizzBuzzModel = new FizzBuzzModel() { Number = inputpostModel.Number };
            fizzBuzzModel.Page = (this.TempData["FizzBuzzPage"] != null) ? TempData["FizzBuzzPage"] as Paging : new Paging();
            fizzBuzzModel.Page.PageIndex = (fizzBuzzModel.Page.PageIndex == 1) ? fizzBuzzModel.Page.PageIndex : (fizzBuzzModel.Page.PageIndex - 1);

            if (ModelState.IsValid) 
            { 
                this.FillNumberList(fizzBuzzModel); 
            }

            this.TempData["FizzBuzzPage"] = fizzBuzzModel.Page;
            return this.View("FizzBuzzView", fizzBuzzModel);
        }

        /// <summary>
        /// Action method to render on Next button click.
        /// </summary>
        /// <param name="inputpostModel">the input postModel</param>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "Next")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzViewNext(FizzBuzzModel inputpostModel)
        {
            FizzBuzzModel fizzBuzzModel = new FizzBuzzModel() { Number = inputpostModel.Number };
            fizzBuzzModel.Page = (this.TempData["FizzBuzzPage"] != null) ? TempData["FizzBuzzPage"] as Paging : new Paging();
            fizzBuzzModel.Page.PageIndex = (fizzBuzzModel.Page.PageIndex == fizzBuzzModel.Page.PageCount) ? fizzBuzzModel.Page.PageIndex : (fizzBuzzModel.Page.PageIndex + 1);

            if (ModelState.IsValid) 
            { 
                this.FillNumberList(fizzBuzzModel); 
            }

            this.TempData["FizzBuzzPage"] = fizzBuzzModel.Page;
            return this.View("FizzBuzzView", fizzBuzzModel);
        }

        /// <summary>
        /// Get List
        /// </summary>
        /// <param name="numbers">parameter numbers</param>
        /// <returns>return FizzBuzzBusinessRule list </returns>
        public List<BaseFizzBuzz> GetList(List<int> numbers)
        {
            List<BaseFizzBuzz> lstFizzBuzzBusinessRule = new List<BaseFizzBuzz>();
            foreach (int number in numbers)
            {
                BaseFizzBuzz fizzBuzzBusinessRule = this.FizzBuzzBusiness.ExecuteBusinessRule(this.Instances, number);
                lstFizzBuzzBusinessRule.Add(fizzBuzzBusinessRule);
            }

            return lstFizzBuzzBusinessRule;
        }

        /// <summary>
        /// fill number list
        /// </summary>
        /// <param name="inputData">input InputData</param>
        private void FillNumberList(FizzBuzzModel inputData)
        {
            var fizzBuzzModels = new List<FizzBuzzModel>();
            
            var numbers = this.FizzBuzzStrategic.GetNumbers(inputData.Number.Value);
            List<BaseFizzBuzz> list = this.GetList(numbers);
            
            foreach (BaseFizzBuzz fizzBuzzRule in list)
            {
                FizzBuzzModel fizzBuzzModel = new FizzBuzzModel();

                fizzBuzzModel.Message = fizzBuzzRule.Message;
                fizzBuzzModel.MessageColor = fizzBuzzRule.MessageColor;

                fizzBuzzModels.Add(fizzBuzzModel);
            }

            inputData.DivisionList = fizzBuzzModels;
            var startndex = (inputData.Page.PageIndex - 1) * FizzBuzzModel.PageSize;
            inputData.DivisionList = inputData.DivisionList.GetRange(startndex, inputData.Number.Value - startndex < FizzBuzzModel.PageSize ? inputData.Number.Value - startndex : FizzBuzzModel.PageSize);
        }
    }
}
