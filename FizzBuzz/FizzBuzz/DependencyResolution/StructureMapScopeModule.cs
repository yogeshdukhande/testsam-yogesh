//-----------------------------------------------------------------------
// <copyright file="StructureMapScopeModule.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------
namespace FizzBuzz.DependencyResolution 
{
    using System.Web;

    using FizzBuzz.App_Start;

    using StructureMap.Web.Pipeline;

    /// <summary>
    /// Definition of StructureMapScopeModule class
    /// </summary>
    public class StructureMapScopeModule : IHttpModule 
    {
        #region Public Methods and Operators
        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose() 
        {
        }

        /// <summary>
        /// method initially called
        /// </summary>
        /// <param name="context">parameter context</param>
        public void Init(HttpApplication context) 
        {
            context.BeginRequest += (sender, e) => StructuremapMvc.StructureMapDependencyScope.CreateNestedContainer();
            context.EndRequest += (sender, e) => 
            {
                HttpContextLifecycle.DisposeAndClearAll();
                StructuremapMvc.StructureMapDependencyScope.DisposeNestedContainer();
            };
        }

        #endregion
    }
}