//-----------------------------------------------------------------------
// <copyright file="StructuremapMvc.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

using FizzBuzz.App_Start;

using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(StructuremapMvc), "Start")]
[assembly: ApplicationShutdownMethod(typeof(StructuremapMvc), "End")]

namespace FizzBuzz.App_Start 
{
    using System.Web.Mvc;
    using FizzBuzz.DependencyResolution;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using StructureMap;

    /// <summary>
    /// Definition of StructureMapMVC class
    /// </summary>
    public static class StructuremapMvc
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets StructureMapDependencyScope
        /// </summary>
        public static StructureMapDependencyScope StructureMapDependencyScope { get; set; }

        #endregion		
        
        #region Public Methods and Operators
        /// <summary>
        /// End Method
        /// </summary>
        public static void End() 
        {
            StructureMapDependencyScope.Dispose();
        }
        
        /// <summary>
        /// Start method
        /// </summary>
        public static void Start() 
        {
            IContainer container = IoC.Initialize();
            StructureMapDependencyScope = new StructureMapDependencyScope(container);
            DependencyResolver.SetResolver(StructureMapDependencyScope);
            DynamicModuleUtility.RegisterModule(typeof(StructureMapScopeModule));
        }

        #endregion
    }
}