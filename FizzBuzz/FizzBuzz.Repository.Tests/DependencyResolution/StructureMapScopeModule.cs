//-----------------------------------------------------------------------
// <copyright file="StructureMapScopeModule.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------
namespace FizzBuzz.Repository.Tests.DependencyResolution 
{
    using System.Web;

    using FizzBuzz.Repository.Tests.App_Start;

    using StructureMap.Web.Pipeline;

    /// <summary>
    /// Initializes a new instance of the StructureMapScopeModule class.
    /// </summary>
    public class StructureMapScopeModule : IHttpModule 
    {
        #region Public Methods and Operators
        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose() 
        {
        }

        /// <summary>
        /// Initialize method
        /// </summary>
        /// <param name="context">parameter context</param>
        public void Init(HttpApplication context) 
        {
            context.BeginRequest += (sender, e) => StructuremapMVC.StructureMapDependencyScope.CreateNestedContainer();
            context.EndRequest += (sender, e) => 
            {
                HttpContextLifecycle.DisposeAndClearAll();
                StructuremapMVC.StructureMapDependencyScope.DisposeNestedContainer();
            };
        }

        #endregion
    }
}