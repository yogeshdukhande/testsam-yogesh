//-----------------------------------------------------------------------
// <copyright file="IoC.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Repository.Tests.DependencyResolution 
{
    using StructureMap;
   
    /// <summary>
    /// Initializes a new instance of the IoC class.
    /// </summary>
    public static class IoC 
    {
        /// <summary>
        /// Initialize method
        /// </summary>
        /// <returns>return container</returns>
        public static IContainer Initialize() 
        {
            return new Container(c => c.AddRegistry<DefaultRegistry>());
        }
    }
}