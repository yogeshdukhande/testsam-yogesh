//-----------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------
namespace FizzBuzz.Repository.Tests.DependencyResolution 
{
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
   
    /// <summary>
    /// Initializes a new instance of the DefaultRegistry class.
    /// </summary>
   public class DefaultRegistry : Registry 
   {
        #region Constructors and Destructors
        /// <summary>
        /// Initializes a new instance of the DefaultRegistry class.
        /// </summary>
        public DefaultRegistry() 
        {
            this.Scan(
                scan => 
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.With(new ControllerConvention());
                });
        }

        #endregion
    }
}