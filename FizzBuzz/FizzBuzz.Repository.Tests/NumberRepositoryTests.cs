﻿//-----------------------------------------------------------------------
// <copyright file="NumberRepositoryTests.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Repository.Tests
{
    using System;
    using System.Collections.Generic;
    using FizzBuzz.Repository;
    using FizzBuzzBusinessLayer;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Initializes a new instance of the NumberRepositoryTests class.
    /// </summary>
    [TestFixture]
    public class NumberRepositoryTests
    {
        /// <summary>
        /// Get Numbers
        /// </summary>
        [Test]
        public void GetNumbers()
        {
            NumberRepository objNumberRepository = new NumberRepository();
            List<int> lstNumber = objNumberRepository.GetNumbers(20);

            Assert.AreEqual("5", lstNumber[4].ToString());
            Assert.AreEqual("20", lstNumber[19].ToString());
        }        
    }
}
