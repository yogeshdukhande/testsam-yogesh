﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzz.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;

    /// <summary>
    /// Definition of FizzBuzz class
    /// </summary>
    public class FizzBuzz : BaseFizzBuzz
    {
        /// <summary>
        /// check whether number is divisible by 3 and 5
        /// </summary>
        /// <param name="inputNumber">input Number</param>
        /// <returns>return boolean</returns>
        public override bool IsDivisible(int inputNumber)
        {
            return ((inputNumber % 3) == 0 && (inputNumber % 5) == 0) ? true : false;
        }

        /// <summary>
        /// set message based on day
        /// </summary>
        /// <param name="inputisSpecificDay">input IsSpecificDay</param>
        public override void SetMessage(bool inputisSpecificDay)
        {
            this.Message = inputisSpecificDay ? "Wizz Wuzz" : "Fizz Buzz";
        }

        /// <summary>
        /// to set message
        /// </summary>
        public override void SetMessageColor()
        {
            this.MessageColor = string.Empty;
        }
    }
}
