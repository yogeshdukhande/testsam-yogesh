﻿//-----------------------------------------------------------------------
// <copyright file="Fizz.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;

    /// <summary>
    /// Definition of Fizz Class
    /// </summary>
    public class Fizz : BaseFizzBuzz
    {
        /// <summary>
        /// check whether number is divisible by 3
        /// </summary>
        /// <param name="inputNumber">input inputNumber</param>
        /// <returns>return boolean</returns>
        public override bool IsDivisible(int inputNumber)
        {
            return (inputNumber % 3) == 0 ? true : false;
        }

        /// <summary>
        /// set message based on specific day
        /// </summary>
        /// <param name="inputisSpecificDay">input IsSpecificDay</param>
        public override void SetMessage(bool inputisSpecificDay)
        {
            this.Message = inputisSpecificDay ? "Wizz" : "Fizz";
        }

        /// <summary>
        /// to set message
        /// </summary>
        public override void SetMessageColor()
        {
            this.MessageColor = "blue";
        }
    }
}
