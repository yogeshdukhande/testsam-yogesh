﻿//-----------------------------------------------------------------------
// <copyright file="BaseFizzBuzz.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------
namespace FizzBuzzBusinessLayer
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Definition of FizzBuzzBusinessRule class
    /// </summary>
    public abstract class BaseFizzBuzz
    {
        /// <summary>
        /// Gets or sets Message
        /// </summary>
        public string Message
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets MessageColor
        /// </summary>
        public string MessageColor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Number
        /// </summary>
        public int? Number
        {
            get;
            set;
        }

        /// <summary>
        /// check whether number is divisible
        /// </summary>
        /// <param name="inputNumber">input number</param>
        /// <returns>return boolean</returns>
        public abstract bool IsDivisible(int inputNumber);

        /// <summary>
        /// to set message
        /// </summary>
        /// <param name="inputisSpecificDay">input IsSpecificDay</param>
        public abstract void SetMessage(bool inputisSpecificDay);
        
        /// <summary>
        /// to set message
        /// </summary>
        public abstract void SetMessageColor();
    }
}
