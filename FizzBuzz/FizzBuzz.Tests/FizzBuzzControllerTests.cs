﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzControllerTests.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.Controllers;
    using FizzBuzz.Models;
    using FizzBuzz.Repository;
    using FizzBuzzBusinessLayer;
    using Moq;
    using NUnit.Framework;
    using StructureMap;
    using StructureMap.Graph;

    /// <summary>
    /// Definition of FizzBuzzControllerTests class
    /// </summary>
    [TestFixture]
    public class FizzBuzzControllerTests
    {
        /// <summary>
        /// Define Container
        /// </summary>
        private IContainer container;

        /// <summary>
        /// show fizzBuzz view
        /// </summary>
        [Test]
        public void Should_Give_FizzBuzzView_OnRequest()
        {
            var mockRepository = new Mock<IRepository>();
            var mockFizzBuzzBusiness = new Mock<FizzBuzzBusinessLogic>();
            var container = new Container();

            IEnumerable<BaseFizzBuzz> fizzBuzzinstances = container.GetAllInstances<BaseFizzBuzz>();
            var controller = new FizzBuzzController(mockRepository.Object, fizzBuzzinstances, mockFizzBuzzBusiness.Object);

            var result = controller.FizzBuzzView() as ViewResult;

            Assert.AreEqual("FizzBuzzView", result.ViewName);
        }

        /// <summary>
        /// show 20 items on page
        /// </summary>      
        [Test]
        public void Should_Show_Items_On_One_Page()
        {
            var repoResponse = new List<int>();

            for (var counter = 1; counter <= 21; counter++)
            {
                repoResponse.Add(counter);
            }

            var mockRepository = new Mock<IRepository>();

            mockRepository.Setup(x => x.GetNumbers(It.IsAny<int>())).Returns(repoResponse);

            IEnumerable<BaseFizzBuzz> fizzBuzzinstances = this.container.GetAllInstances<BaseFizzBuzz>();
            var mockFizzBuzzBusiness = new Mock<FizzBuzzBusinessLogic>();
            var controller = new FizzBuzzController(mockRepository.Object, fizzBuzzinstances, mockFizzBuzzBusiness.Object);

            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 21 }) as ViewResult;

            var viewModel = (FizzBuzzModel)result.Model;

            Assert.AreEqual(20, viewModel.DivisionList.Count);
        }
        
        /// <summary>
        /// should implement PageCount
        /// </summary>
        [Test]
        public void Should_implement_PageCount()
        {
            var repoResponse = new List<int>();

            for (var counter = 1; counter <= 21; counter++)
            {
                repoResponse.Add(counter);
            }

            var mockRepository = new Mock<IRepository>();

            mockRepository.Setup(x => x.GetNumbers(It.IsAny<int>())).Returns(repoResponse);

            IEnumerable<BaseFizzBuzz> fizzBuzzinstances = this.container.GetAllInstances<BaseFizzBuzz>();
            var mockFizzBuzzBusiness = new Mock<FizzBuzzBusinessLogic>();
            var controller = new FizzBuzzController(mockRepository.Object, fizzBuzzinstances, mockFizzBuzzBusiness.Object);

            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 63 }) as ViewResult;

            var viewModel = (FizzBuzzModel)result.Model;

            Assert.AreEqual(4, viewModel.Page.PageCount);
            }

        /// <summary>
        /// should implement paging
        /// </summary>
        [Test]
        public void Should_implement_paging()
        {
            var repoResponse = new List<int>();

            for (var counter = 1; counter <= 21; counter++)
            {
                repoResponse.Add(counter);
            }

            var mockRepository = new Mock<IRepository>();

            mockRepository.Setup(x => x.GetNumbers(It.IsAny<int>())).Returns(repoResponse);
            
            IEnumerable<BaseFizzBuzz> fizzBuzzinstances = this.container.GetAllInstances<BaseFizzBuzz>();
            var mockFizzBuzzBusiness = new Mock<FizzBuzzBusinessLogic>();
            var controller = new FizzBuzzController(mockRepository.Object, fizzBuzzinstances, mockFizzBuzzBusiness.Object);

            var result = controller.FizzBuzzViewNext(new FizzBuzzModel { Page = new Paging() { PageIndex = 2 }, Number = 21 }) as ViewResult;

            var viewModel = (FizzBuzzModel)result.Model;

            Assert.AreEqual(1, viewModel.DivisionList.Count);
        }

        /// <summary>
        /// Setup method
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.container = new Container(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.Assembly("FizzBuzzBusinessLayer");
                    scan.AddAllTypesOf<BaseFizzBuzz>();
                });
            });
        }
         
        /// <summary>
        /// should return List
        /// </summary>
        /// <param name="inputrows">input rows</param>
        /// <returns>return IFizzBuzz List</returns>
        private List<BaseFizzBuzz> GetFizzBuzzList(int inputrows)
        {
            List<BaseFizzBuzz> list = new List<BaseFizzBuzz>();
            for (int index = 1; index <= inputrows; index++)
            {
                SimpleDivision simpleDivision = new SimpleDivision();
                simpleDivision.IsDivisible(index);
                list.Add(simpleDivision);
            }

            return list;
        }
    }
}
