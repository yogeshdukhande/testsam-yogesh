//-----------------------------------------------------------------------
// <copyright file="StructuremapMvc.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------
using FizzBuzzBusinessLayer.Tests.App_Start;

using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(StructuremapMVC), "Start")]
[assembly: ApplicationShutdownMethod(typeof(StructuremapMVC), "End")]

namespace FizzBuzzBusinessLayer.Tests.App_Start 
{
    using System.Web.Mvc;
    using FizzBuzzBusinessLayer.Tests.DependencyResolution;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using StructureMap;

    /// <summary>
    /// Initializes a new instance of the StructureMapMVC class.
    /// </summary>
    public static class StructuremapMVC 
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets StructureMapDependencyScope
        /// </summary>
        public static StructureMapDependencyScope StructureMapDependencyScope { get; set; }

        #endregion
    
        #region Public Methods and Operators
        /// <summary>
        /// End method
        /// </summary>
        public static void End()
        {
            StructureMapDependencyScope.Dispose();
        }

        /// <summary>
        /// Start method
        /// </summary>
        public static void Start() 
        {
            IContainer container = IoC.Initialize();
            StructureMapDependencyScope = new StructureMapDependencyScope(container);
            DependencyResolver.SetResolver(StructureMapDependencyScope);
            DynamicModuleUtility.RegisterModule(typeof(StructureMapScopeModule));
        }

        #endregion
    }
}