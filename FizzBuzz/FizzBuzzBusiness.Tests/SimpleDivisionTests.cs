﻿//-----------------------------------------------------------------------
// <copyright file="SimpleDivisionTests.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.Controllers;
    using FizzBuzz.Models;
    using FizzBuzz.Repository;
    using FizzBuzzBusinessLayer;
    using Moq;
    using NUnit.Framework;
    using StructureMap;
    using StructureMap.Graph;

    /// <summary>
    /// Definition of SimpleDivisionTests class
    /// </summary>
    [TestFixture]
    public class SimpleDivisionTests
    {
        /// <summary>
        /// Define Container
        /// </summary>
        private IContainer container;

        /// <summary>
        /// count of numbers should be same as input number
        /// </summary>
        [Test]
        public void Should_show_twentycount()
        {
            var repoResponse = new List<int>();

            for (var counter = 1; counter <= 20; counter++)
            {
                repoResponse.Add(counter);
            }

            var mockRepository = new Mock<IRepository>();

            mockRepository.Setup(x => x.GetNumbers(It.IsAny<int>())).Returns(repoResponse);
            
            IEnumerable<BaseFizzBuzz> fizzBuzzinstances = this.container.GetAllInstances<BaseFizzBuzz>();
            var mockFizzBuzzBusiness = new Mock<FizzBuzzBusinessLogic>();
            var controller = new FizzBuzzController(mockRepository.Object, fizzBuzzinstances, mockFizzBuzzBusiness.Object);

            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 20 }) as ViewResult;

            var viewModel = (FizzBuzzModel)result.Model;

            Assert.AreEqual(20, viewModel.DivisionList.Count);
            Assert.AreEqual("1", ((FizzBuzzModel)viewModel.DivisionList[0]).Message);
            Assert.AreEqual("2", ((FizzBuzzModel)viewModel.DivisionList[1]).Message);
        }

        /// <summary>
        /// Setup method
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            this.container = new Container(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.Assembly("FizzBuzzBusinessLayer");
                    scan.AddAllTypesOf<BaseFizzBuzz>();
                });
            });
        }
    }
}
