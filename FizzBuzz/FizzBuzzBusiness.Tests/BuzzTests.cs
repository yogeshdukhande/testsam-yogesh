﻿//-----------------------------------------------------------------------
// <copyright file="BuzzTests.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.Controllers;
    using FizzBuzz.Models;
    using FizzBuzz.Repository;
    using FizzBuzzBusinessLayer;
    using Moq;
    using NUnit.Framework;
    using StructureMap;
    using StructureMap.Graph;

    /// <summary>
    /// Definition of BuzzTests class
    /// </summary>
    [TestFixture]
    public class BuzzTests
    {
        /// <summary>
        /// Define Container
        /// </summary>
        private IContainer container;

        /// <summary>
        /// if number is divisible by 5 then replace with proper message
        /// </summary>
        [Test]
        public void Should_Show_Proper_Message_Messagecolor()
        {
            var repoResponse = new List<int>();

            for (var counter = 1; counter <= 20; counter++)
            {
                repoResponse.Add(counter);
            }

            var mockRepository = new Mock<IRepository>();

            mockRepository.Setup(x => x.GetNumbers(It.IsAny<int>())).Returns(repoResponse);

            IEnumerable<BaseFizzBuzz> fizzBuzzinstances = this.container.GetAllInstances<BaseFizzBuzz>();
            var mockFizzBuzzBusiness = new Mock<FizzBuzzBusinessLogic>();
            var controller = new FizzBuzzController(mockRepository.Object, fizzBuzzinstances, mockFizzBuzzBusiness.Object);

            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 20 }) as ViewResult;

            var viewModel = (FizzBuzzModel)result.Model;

            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wuzz", ((FizzBuzzModel)viewModel.DivisionList[4]).Message);
                Assert.AreEqual("Wuzz", ((FizzBuzzModel)viewModel.DivisionList[9]).Message);
            }
            else
            {
                Assert.AreEqual("Buzz", ((FizzBuzzModel)viewModel.DivisionList[4]).Message);
                Assert.AreEqual("Buzz", ((FizzBuzzModel)viewModel.DivisionList[9]).Message);
            }

            Assert.AreEqual("green", ((FizzBuzzModel)viewModel.DivisionList[4]).MessageColor);
            Assert.AreEqual("green", ((FizzBuzzModel)viewModel.DivisionList[9]).MessageColor);
        }

        /// <summary>
        /// Setup method
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            this.container = new Container(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.Assembly("FizzBuzzBusinessLayer");
                    scan.AddAllTypesOf<BaseFizzBuzz>();
                });
            });
        }
    }
}
