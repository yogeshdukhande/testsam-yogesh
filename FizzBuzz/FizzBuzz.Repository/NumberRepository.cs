﻿//-----------------------------------------------------------------------
// <copyright file="NumberRepository.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------
namespace FizzBuzz.Repository
{
    using System;
    using System.Collections.Generic;
    using StructureMap;

    /// <summary>
    /// The business logic of the application
    /// </summary>
    public class NumberRepository : IRepository
    {
        /// <summary>
        /// Gets the list of numbers from one up to the number given.
        /// </summary>
        /// <param name="inputnumber">The input number.</param>
        /// <returns>List of numbers</returns>
        public List<int> GetNumbers(int inputnumber)
        {
            List<int> list = new List<int>();
            
            for (var counter = 1; counter <= inputnumber; counter++)
            {
                list.Add(counter);
            }

            return list;
        }

        /// <summary>
        /// get whether today is wednesday
        /// </summary>
        /// <returns>return boolean</returns>
        public bool IsSpecificDay()
        {
            return DayOfWeek.Wednesday == System.DateTime.Now.DayOfWeek ? true : false;
        }
    }
}
