﻿//-----------------------------------------------------------------------
// <copyright file="IRepository.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------
namespace FizzBuzz.Repository
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Abstraction of Strategic logic
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Gets the list of numbers from one up to the number given.
        /// </summary>
        /// <param name="inputnumber">The input number.</param>
        /// <returns>List of IDivision</returns>
        List<int> GetNumbers(int inputnumber);

        /// <summary>
        /// check is specific day
        /// </summary>
        /// <returns>return boolean</returns>
        bool IsSpecificDay();
    }
}
